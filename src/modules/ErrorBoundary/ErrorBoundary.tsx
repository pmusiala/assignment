import React from 'react';

type State = {
    hasError: boolean;
}
class ErrorBoundary extends React.Component<React.PropsWithChildren, State> {
    state = {
        hasError: false
    };

    componentDidCatch(): void {
        this.setState({ hasError: true });
    }

    render() {
        if (this.state.hasError) return <>Something went wrong</>;

        return this.props.children;
    }
}

export default ErrorBoundary;
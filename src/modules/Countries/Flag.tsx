import { CountryCode } from './countries';

type Props = {
    countryCode: CountryCode;
}

const Flag: React.FC<Props> = ({ countryCode }) => {
    return <div className="drop-shadow"><img src={`https://www.countryflagicons.com/FLAT/64/${countryCode}.png`}/></div>;
};

export default Flag;
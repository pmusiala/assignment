import { screen, render } from '@testing-library/react';
import MatchScore from './MatchScore';
import { firstMatch } from '../../../fixtures/matches';

describe('MatchScore', () => {
    it('should render teams names and scores', () => {
        render(<MatchScore match={firstMatch} />);

        expect(screen.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(screen.getByTestId('home-score')).toHaveTextContent('2');
        expect(screen.getByTestId('away-country')).toHaveTextContent('Brazil');
        expect(screen.getByTestId('away-score')).toHaveTextContent('1');
    });
});
import type { Match } from '../../../interfaces/Match';
import Flag from '../../Countries/Flag';
import { countries } from '../../Countries/countries';

type Props = {
    match: Match;
}

const MatchScore: React.FC<Props> = ({ match }) => <>
    <div className="col-span-2 justify-self-end" data-testid="home-country">{countries[match.home.countryCode]}</div>
    <div className="col-span-1 justify-self-center"><Flag countryCode={match.home.countryCode} /></div>
    <div className="col-span-1 justify-self-center font-bold text-xl">
        <div className="inline rounded bg-sportradar p-1 m-2 text-white" data-testid="home-score">{match.home.score}</div>
        :
        <div className="inline rounded bg-sportradar p-1 m-2 text-white" data-testid="away-score">{match.away.score}</div>
    </div>
    <div className="col-span-1 justify-self-center"><Flag countryCode={match.away.countryCode} /></div>
    <div className="col-span-2 justify-self-start" data-testid="away-country">{countries[match.away.countryCode]}</div>
</>;

export default MatchScore;
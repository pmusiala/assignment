import { screen, render } from '@testing-library/react';
import { within } from '@testing-library/dom';
import Board from './Board';
import userEvent from '@testing-library/user-event';
import { firstMatch, secondMatch } from '../../fixtures/matches';

describe('Board', () => {
    it('should render without registered ', () => {
        render(<Board initialData={{}} />);
    });

    it('should start new match', async () => {
        render(<Board initialData={{
        }} />);

        const selectHomeTeam = within(screen.getByTestId('home-team-selector')).getByTestId('country-selector');
        const selectAwayTeam = within(screen.getByTestId('away-team-selector')).getByTestId('country-selector');
        const startMatchButton = screen.getByTestId('start-match');

        await userEvent.selectOptions(selectHomeTeam, 'DE');
        await userEvent.selectOptions(selectAwayTeam, 'BR');
        await userEvent.click(startMatchButton);

        const withinSummary = within(screen.getByTestId('summary'));
        const withinFirstMatchBeforeUpdate = within(withinSummary.getByTestId('match-0'));
        expect(withinFirstMatchBeforeUpdate.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(withinFirstMatchBeforeUpdate.getByTestId('home-score')).toHaveTextContent('0');
        expect(withinFirstMatchBeforeUpdate.getByTestId('away-country')).toHaveTextContent('Brazil');
        expect(withinFirstMatchBeforeUpdate.getByTestId('away-score')).toHaveTextContent('0');
    });


    it('should add second match on top', async () => {
        render(<Board initialData={{
        }} />);

        const selectHomeTeam = within(screen.getByTestId('home-team-selector')).getByTestId('country-selector');
        const selectAwayTeam = within(screen.getByTestId('away-team-selector')).getByTestId('country-selector');
        const startMatchButton = screen.getByTestId('start-match');

        await userEvent.selectOptions(selectHomeTeam, 'DE');
        await userEvent.selectOptions(selectAwayTeam, 'BR');
        await userEvent.click(startMatchButton);

        await userEvent.selectOptions(selectHomeTeam, 'CA');
        await userEvent.selectOptions(selectAwayTeam, 'FR');
        await userEvent.click(startMatchButton);

        const withinSummary = within(screen.getByTestId('summary'));
        const withinFirstMatchBeforeUpdate = within(withinSummary.getByTestId('match-0'));
        expect(withinFirstMatchBeforeUpdate.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(withinFirstMatchBeforeUpdate.getByTestId('home-score')).toHaveTextContent('0');
        expect(withinFirstMatchBeforeUpdate.getByTestId('away-country')).toHaveTextContent('France');
        expect(withinFirstMatchBeforeUpdate.getByTestId('away-score')).toHaveTextContent('0');
    });

    it('should change match order in summary depending on score', async () => {
        render(<Board initialData={{
            [firstMatch.id]: { ...firstMatch, home: { countryCode: 'DE', score: 1 }, creationDate: Date.now() },
            [secondMatch.id]: { ...secondMatch, creationDate: Date.now() - 1 }
        }} />);

        const withinSummary = within(screen.getByTestId('summary'));

        const withinFirstMatchBeforeUpdate = within(withinSummary.getByTestId('match-0'));
        expect(withinFirstMatchBeforeUpdate.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(withinFirstMatchBeforeUpdate.getByTestId('home-score')).toHaveTextContent('1');
        expect(withinFirstMatchBeforeUpdate.getByTestId('away-country')).toHaveTextContent('Brazil');
        expect(withinFirstMatchBeforeUpdate.getByTestId('away-score')).toHaveTextContent('1');

        const withinSecondMatchBeforeUpdate = within(withinSummary.getByTestId('match-1'));
        expect(withinSecondMatchBeforeUpdate.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(withinSecondMatchBeforeUpdate.getByTestId('home-score')).toHaveTextContent('1');
        expect(withinSecondMatchBeforeUpdate.getByTestId('away-country')).toHaveTextContent('France');
        expect(withinSecondMatchBeforeUpdate.getByTestId('away-score')).toHaveTextContent('1');

        const homeScoredButton = within(screen.getByTestId('controls-match-0')).getByTestId('increase-home-score');

        await userEvent.click(homeScoredButton);

        const withinFirstMatchAfterUpdate = within(withinSummary.getByTestId('match-0'));
        const withinSecondMatchAfterUpdate = within(withinSummary.getByTestId('match-1'));

        expect(withinFirstMatchAfterUpdate.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(withinFirstMatchAfterUpdate.getByTestId('home-score')).toHaveTextContent('2');
        expect(withinFirstMatchAfterUpdate.getByTestId('away-country')).toHaveTextContent('France');
        expect(withinFirstMatchAfterUpdate.getByTestId('away-score')).toHaveTextContent('1');

        expect(withinSecondMatchAfterUpdate.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(withinSecondMatchAfterUpdate.getByTestId('home-score')).toHaveTextContent('1');
        expect(withinSecondMatchAfterUpdate.getByTestId('away-country')).toHaveTextContent('Brazil');
        expect(withinSecondMatchAfterUpdate.getByTestId('away-score')).toHaveTextContent('1');
    });

    it('should change match order in summary depending on creation date', async () => {
        render(<Board initialData={{
            [firstMatch.id]: { ...firstMatch, home: { countryCode: 'DE', score: 1 }, creationDate: Date.now() },
            [secondMatch.id]: { ...secondMatch, home: { countryCode: 'CA', score: 2 }, creationDate: Date.now() - 1 }
        }} />);

        const withinSummary = within(screen.getByTestId('summary'));

        const withinFirstMatchBeforeUpdate = within(withinSummary.getByTestId('match-0'));
        expect(withinFirstMatchBeforeUpdate.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(withinFirstMatchBeforeUpdate.getByTestId('home-score')).toHaveTextContent('2');
        expect(withinFirstMatchBeforeUpdate.getByTestId('away-country')).toHaveTextContent('France');
        expect(withinFirstMatchBeforeUpdate.getByTestId('away-score')).toHaveTextContent('1');

        const withinSecondMatchBeforeUpdate = within(withinSummary.getByTestId('match-1'));
        expect(withinSecondMatchBeforeUpdate.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(withinSecondMatchBeforeUpdate.getByTestId('home-score')).toHaveTextContent('1');
        expect(withinSecondMatchBeforeUpdate.getByTestId('away-country')).toHaveTextContent('Brazil');
        expect(withinSecondMatchBeforeUpdate.getByTestId('away-score')).toHaveTextContent('1');

        const homeScoredButton = within(screen.getByTestId('controls-match-1')).getByTestId('increase-home-score');
        await userEvent.click(homeScoredButton);

        const withinFirstMatchAfterUpdate = within(withinSummary.getByTestId('match-0'));
        expect(withinFirstMatchAfterUpdate.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(withinFirstMatchAfterUpdate.getByTestId('home-score')).toHaveTextContent('2');
        expect(withinFirstMatchAfterUpdate.getByTestId('away-country')).toHaveTextContent('Brazil');
        expect(withinFirstMatchAfterUpdate.getByTestId('away-score')).toHaveTextContent('1');

        const withinSecondMatchAfterUpdate = within(withinSummary.getByTestId('match-1'));
        expect(withinSecondMatchAfterUpdate.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(withinSecondMatchAfterUpdate.getByTestId('home-score')).toHaveTextContent('2');
        expect(withinSecondMatchAfterUpdate.getByTestId('away-country')).toHaveTextContent('France');
        expect(withinSecondMatchAfterUpdate.getByTestId('away-score')).toHaveTextContent('1');
    });

    it('should sort matches by creation date in control panel', () => {
        render(<Board initialData={{
            [firstMatch.id]: { ...firstMatch, creationDate: Date.now() },
            [secondMatch.id]: { ...secondMatch, creationDate: Date.now() - 1 }
        }} />);

        const firstMatchControls = within(screen.getByTestId('controls-match-0'));
        const secondMatchControls = within(screen.getByTestId('controls-match-1'));

        expect(firstMatchControls.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(firstMatchControls.getByTestId('away-country')).toHaveTextContent('France');

        expect(secondMatchControls.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(secondMatchControls.getByTestId('away-country')).toHaveTextContent('Brazil');
    });

    it('should not change match order when score changes', async () => {
        render(<Board initialData={{
            [firstMatch.id]: { ...firstMatch, creationDate: Date.now() },
            [secondMatch.id]: { ...secondMatch, creationDate: Date.now() - 1 }
        }} />);

        const firstMatchControlsBeforeUpdate = within(screen.getByTestId('controls-match-0'));
        expect(firstMatchControlsBeforeUpdate.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(firstMatchControlsBeforeUpdate.getByTestId('away-country')).toHaveTextContent('France');

        const secondMatchControlsBeforeUpdate = within(screen.getByTestId('controls-match-1'));
        expect(secondMatchControlsBeforeUpdate.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(secondMatchControlsBeforeUpdate.getByTestId('away-country')).toHaveTextContent('Brazil');

        const updateScoreButton = secondMatchControlsBeforeUpdate.getByTestId('increase-home-score');
        await userEvent.click(updateScoreButton);
        await userEvent.click(updateScoreButton);

        const firstMatchControlsAfterUpdate = within(screen.getByTestId('controls-match-0'));
        expect(firstMatchControlsAfterUpdate.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(firstMatchControlsAfterUpdate.getByTestId('away-country')).toHaveTextContent('France');

        const secondMatchControlsAfterUpdate = within(screen.getByTestId('controls-match-1'));
        expect(secondMatchControlsAfterUpdate.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(secondMatchControlsAfterUpdate.getByTestId('away-country')).toHaveTextContent('Brazil');
    });
});
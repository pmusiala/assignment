import { useReducer } from 'react';
import Summary from './Summary/Summary';
import Controls from './Controls/Controls';
import { CountryCode } from '../Countries/countries';
import { BoardData, reducer } from './state';

type Props = {
    initialData: BoardData;
}

const Board: React.FC<Props> = ({ initialData }) => {
    const [state, dispatch] = useReducer(reducer, initialData);

    const sortedByScoreAndCreationDate = Object.values(state).sort((a, b) => {
        const aSum = a.away.score + a.home.score;
        const bSum = b.away.score + b.home.score;

        return bSum - aSum || b.creationDate - a.creationDate;
    });

    const sortedByCreationDate = Object.values(state).sort((a, b) => {
        return a.creationDate - b.creationDate;
    });

    return <div>
        <div data-testid="summary">
            <Summary matchList={sortedByScoreAndCreationDate}></Summary>
        </div>

        <div className="bg-blue-100 p-2 m-5" data-testid="control-panel">
            <div className="text-xl p-2 m-2">
                Control panel
            </div>
            <Controls matchList={sortedByCreationDate}
                finishMatch={(matchId: string) => dispatch({
                    type: 'MATCH_FINISHED',
                    payload: {
                        matchId
                    }
                })}
                increaseScore={(matchId: string, team: 'home' | 'away') => dispatch({
                    type: 'TEAM_SCORED',
                    payload: {
                        matchId,
                        team
                    }
                })}
                startMatch={(home: CountryCode, away: CountryCode) => dispatch({
                    type: 'MATCH_STARTED',
                    payload: {
                        home, away
                    }
                })}
            ></Controls>
        </div>
    </div>;
};

export default Board;
import React from 'react';

const Button: React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = ({ ...props }) => {
    const { children, className, ...other } = props;
    return <button className={`col-span-1 rounded text-white p-2 font-bold hover:opacity-70 ${className}`} {...other}>{children}</button>;
};

export default Button;
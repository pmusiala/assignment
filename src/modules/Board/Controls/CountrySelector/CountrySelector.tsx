import { CountryCode } from '../../../Countries/countries';

type Props = {
    countries: Array<[CountryCode, string]>;
    selectedCountry: CountryCode | null;
    setSelectedCountry: (countryCode: CountryCode) => void;
    placeholder: string;
}

const CountrySelector: React.FC<Props> = ({ countries, selectedCountry, setSelectedCountry, placeholder }) => {
    return (
        <select data-testid="country-selector" onChange={(e) => { setSelectedCountry(e.target.value as CountryCode); }} value={selectedCountry || ''}>
            <option disabled value="">{placeholder}</option>
            {countries.map(([countryCode, countryName]) => {
                return <option key={countryCode} value={countryCode}>{countryName}</option>;
            })}
        </select>
    );
};

export default CountrySelector;
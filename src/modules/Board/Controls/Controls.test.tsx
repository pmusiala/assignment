import { screen, render } from '@testing-library/react';
import Controls from './Controls';
import { vi } from 'vitest';
import { within } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { firstMatch, secondMatch } from '../../../fixtures/matches';

describe('Controls', () => {
    let increaseScore: ReturnType<typeof vi.fn>;
    let finishMatch: ReturnType<typeof vi.fn>;
    let startMatch: ReturnType<typeof vi.fn>;

    beforeEach(() => {
        increaseScore = vi.fn();
        finishMatch = vi.fn();
        startMatch = vi.fn();
    });

    afterAll(() => {
        vi.resetAllMocks();
    });

    it('should call increase score for correct team', async () => {
        render(<Controls matchList={[firstMatch]} increaseScore={increaseScore} finishMatch={finishMatch} startMatch={startMatch} />);

        const withinFirstMatch = within(screen.getByTestId('controls-match-0'));
        const increaseHomeScoreButton = withinFirstMatch.getByTestId('increase-home-score');
        await userEvent.click(increaseHomeScoreButton);

        expect(increaseScore).toBeCalledWith('1', 'home');
    });

    it('should call increase score for correct team when there are multiple teams', async () => {
        render(<Controls matchList={[firstMatch, secondMatch]} increaseScore={increaseScore} finishMatch={finishMatch} startMatch={startMatch} />);

        const withinFirstMatch = within(screen.getByTestId('controls-match-1'));
        const increaseHomeScoreButton = withinFirstMatch.getByTestId('increase-away-score');
        await userEvent.click(increaseHomeScoreButton);

        expect(increaseScore).toBeCalledWith('2', 'away');
    });

    it('should call finish match for correct match', async () => {
        render(<Controls matchList={[firstMatch, secondMatch]} increaseScore={increaseScore} finishMatch={finishMatch} startMatch={startMatch} />);

        const withinFirstMatch = within(screen.getByTestId('controls-match-1'));
        const finishMatchButton = await withinFirstMatch.findByTestId('finish-match');
        await userEvent.click(finishMatchButton);

        expect(finishMatch).toBeCalledWith('2');
    });

    it('should not call start match when teams are not selected', async () => {
        render(<Controls matchList={[firstMatch, secondMatch]} increaseScore={increaseScore} finishMatch={finishMatch} startMatch={startMatch} />);

        const startMatchButton = screen.getByTestId('start-match');

        await userEvent.click(startMatchButton);

        expect(startMatch).not.toBeCalled();
    });

    it('should not call start match when only one team is selected', async () => {
        render(<Controls matchList={[firstMatch, secondMatch]} increaseScore={increaseScore} finishMatch={finishMatch} startMatch={startMatch} />);
        const selectHomeTeam = within(screen.getByTestId('home-team-selector')).getByTestId('country-selector');
        const startMatchButton = screen.getByTestId('start-match');

        await userEvent.selectOptions(selectHomeTeam, 'AR');
        await userEvent.click(startMatchButton);

        expect(startMatch).not.toBeCalled();
    });

    it('should call start match when both teams are selected', async () => {
        render(<Controls matchList={[firstMatch, secondMatch]} increaseScore={increaseScore} finishMatch={finishMatch} startMatch={startMatch} />);
        const selectHomeTeam = within(screen.getByTestId('home-team-selector')).getByTestId('country-selector');
        const selectAwayTeam = within(screen.getByTestId('away-team-selector')).getByTestId('country-selector');
        const startMatchButton = screen.getByTestId('start-match');

        await userEvent.selectOptions(selectHomeTeam, 'US');
        await userEvent.selectOptions(selectAwayTeam, 'AR');
        await userEvent.click(startMatchButton);

        expect(startMatch).toBeCalledWith('US', 'AR');
    });

    it('should filter countries when are already playing or are selected', async () => {
        render(<Controls matchList={[firstMatch, secondMatch]} increaseScore={increaseScore} finishMatch={finishMatch} startMatch={startMatch} />);
        const selectHomeTeam = within(screen.getByTestId('home-team-selector')).getByTestId('country-selector');
        const selectAwayTeam = within(screen.getByTestId('away-team-selector')).getByTestId('country-selector');

        await userEvent.selectOptions(selectHomeTeam, 'US');

        expect( async () => {
            await userEvent.selectOptions(selectAwayTeam, 'US');
        }).rejects.toThrowError();

        expect( async () => {
            await userEvent.selectOptions(selectAwayTeam, 'DE');
        }).rejects.toThrowError();

        expect( async () => {
            await userEvent.selectOptions(selectAwayTeam, 'BR');
        }).rejects.toThrowError();

        expect( async () => {
            await userEvent.selectOptions(selectAwayTeam, 'CA');
        }).rejects.toThrowError();

        expect( async () => {
            await userEvent.selectOptions(selectAwayTeam, 'FR');
        }).rejects.toThrowError();
    });
});

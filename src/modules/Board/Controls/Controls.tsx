import { useState } from 'react';
import { Match } from '../../../interfaces/Match';
import MatchScore from '../MatchScore/MatchScore';
import Button from './Button/Button';
import { CountryCode, countries } from '../../Countries/countries';
import CountrySelector from './CountrySelector/CountrySelector';

type Props = {
    matchList: Array<Match>;
    increaseScore: (matchId: string, team: 'home' | 'away') => void;
    finishMatch: (matchId: string) => void;
    startMatch: (home: CountryCode, away: CountryCode) => void;
}

function getAvailableCountryEntries(usedCountries: CountryCode[]) {
    return Object.entries(countries).filter(([countryCode]) => {
        return !usedCountries.includes(countryCode as CountryCode);
    }) as Array<[CountryCode, string]>;
}

const Controls: React.FC<Props> = ({ matchList, increaseScore, finishMatch, startMatch }) => {
    const [selectedHome, setSelectedHome] = useState<CountryCode | null>(null);
    const [selectedAway, setSelectedAway] = useState<CountryCode | null>(null);

    const usedCountries = Object.values(matchList).reduce((result, match) => {
        result.push(match.home.countryCode, match.away.countryCode);
        return result;
    }, [] as CountryCode[]);

    return (<>
        {matchList.map((match, index) => {
            return <div key={match.id} className="grid grid-cols-10 gap-2 items-center p-2" data-testid={`controls-match-${index}`}>
                <Button onClick={() => increaseScore(match.id, 'home')} className="bg-sportradar " data-testid="increase-home-score">home scored</Button>

                <Button onClick={() => increaseScore(match.id, 'away')} className="bg-sportradar " data-testid="increase-away-score">away scored</Button>

                <Button onClick={() => finishMatch(match.id)} className="bg-red-500 " data-testid="finish-match">finish match</Button>

                <MatchScore match={match} />
            </div>;
        })}

        <div className="w-full grid grid-cols-10 items-center">
            <div className="col-span-4 justify-self-center" data-testid="home-team-selector">
                <CountrySelector
                    placeholder="select home"
                    countries={getAvailableCountryEntries([...usedCountries, selectedAway as CountryCode])}
                    selectedCountry={selectedHome}
                    setSelectedCountry={setSelectedHome} />
            </div>

            <div className="col-span-2 justify-self-center">
                <Button disabled={!selectedHome || !selectedAway} onClick={() => {
                    if (!selectedHome || !selectedAway) { return; }

                    startMatch(selectedHome, selectedAway);
                    setSelectedHome(null);
                    setSelectedAway(null);
                }} className="bg-sportradar" data-testid="start-match">start match</Button>
            </div>

            <div className="col-span-4 justify-self-center" data-testid="away-team-selector">
                <CountrySelector
                    placeholder="select away"
                    countries={getAvailableCountryEntries([...usedCountries, selectedHome as CountryCode])}
                    selectedCountry={selectedAway}
                    setSelectedCountry={setSelectedAway} />
            </div>
        </div>
    </>);
};

export default Controls;

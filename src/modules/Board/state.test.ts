import { reducer } from './state';
import { vi, SpyInstance } from 'vitest';
import * as uuid from 'uuid';

describe('State', () => {
    let uuidv4: SpyInstance;
    beforeEach(() => {
        uuidv4 = vi.spyOn(uuid, 'v4');
    });

    describe('reducer', () => {
        it('should return add new match data on MATCH_STARTED', () => {
            uuidv4.mockReturnValue('1');

            expect(reducer({}, {
                type: 'MATCH_STARTED',
                payload: {
                    home: 'DE',
                    away: 'BR'
                }
            })).toEqual(expect.objectContaining({
                1: expect.objectContaining({
                    home: {
                        countryCode: 'DE',
                        score: 0
                    },
                    away: {
                        countryCode: 'BR',
                        score: 0
                    }
                })
            }));
        });

        it('should update existing match when team scores', () => {
            expect(reducer({
                1: {
                    home: {
                        countryCode: 'DE',
                        score: 0
                    },
                    away: {
                        countryCode: 'BR',
                        score: 0
                    },
                    id: '1',
                    creationDate: Date.now()
                }
            }, {
                type: 'TEAM_SCORED',
                payload: {
                  matchId: '1',
                  team: 'away'
                }
            })).toEqual(expect.objectContaining({
                1: expect.objectContaining({
                    home: {
                        countryCode: 'DE',
                        score: 0
                    },
                    away: {
                        countryCode: 'BR',
                        score: 1
                    },
                    id: '1',
                })
            }));
        });

        it('should remove existing match when match finishes', () => {
            expect(reducer({
                1: {
                    home: {
                        countryCode: 'DE',
                        score: 0
                    },
                    away: {
                        countryCode: 'BR',
                        score: 0
                    },
                    id: '1',
                    creationDate: Date.now()
                }
            }, {
                type: 'MATCH_FINISHED',
                payload: {
                  matchId: '1'
                }
            })).toEqual({});
        });
    });
});
import { v4 as uuidv4 } from 'uuid';
import { Match } from '../../interfaces/Match';
import { CountryCode } from '../Countries/countries';

export type BoardData = {
    [key: string]: Match & { creationDate: number };
}

export type TeamScoredAction = {
    type: 'TEAM_SCORED';
    payload: {
        matchId: string;
        team: 'home' | 'away';
    }
}

export type MatchFinishedAction = {
    type: 'MATCH_FINISHED';
    payload: {
        matchId: string;
    }
}

export type MatchStarted = {
    type: 'MATCH_STARTED';
    payload: {
        home: CountryCode;
        away: CountryCode;
    }
}

export const reducer = (state: BoardData, action: TeamScoredAction | MatchFinishedAction | MatchStarted) => {
    switch (action.type) {
        case 'TEAM_SCORED': {
            const { matchId, team } = action.payload;

            return {
                ...state,
                [matchId]: {
                    ...state[matchId],
                    [team]: {
                        ...state[matchId][team],
                        score: state[matchId][team].score + 1
                    }
                }
            };
        }
        case 'MATCH_FINISHED':
            delete state[action.payload.matchId];
            return { ...state };

        case 'MATCH_STARTED': {
            const id = uuidv4();
            return {
                ...state,
                [id]: {
                    id,
                    home: {
                        countryCode: action.payload.home,
                        score: 0
                    },
                    away: {
                        countryCode: action.payload.away,
                        score: 0
                    },
                    creationDate: Date.now(),

                }
            };
        }
    }

    return state;
};
import { screen, render } from '@testing-library/react';
import { within } from '@testing-library/dom';
import Summary from './Summary';
import { firstMatch, secondMatch } from '../../../fixtures/matches';

describe('Summary', () => {
    it('should render teams names and scores', () => {
        render(<Summary matchList={[firstMatch]} />);

        expect(screen.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(screen.getByTestId('home-score')).toHaveTextContent('2');
        expect(screen.getByTestId('away-country')).toHaveTextContent('Brazil');
        expect(screen.getByTestId('away-score')).toHaveTextContent('1');
    });

    it('should render multiple matches', () => {
        render(<Summary matchList={[firstMatch, secondMatch]} />);

        const withinFirstMatch = within(screen.getByTestId('match-0'));
        const withinSecondMatch = within(screen.getByTestId('match-1'));

        expect(withinFirstMatch.getByTestId('home-country')).toHaveTextContent('Germany');
        expect(withinFirstMatch.getByTestId('home-score')).toHaveTextContent('2');
        expect(withinFirstMatch.getByTestId('away-country')).toHaveTextContent('Brazil');
        expect(withinFirstMatch.getByTestId('away-score')).toHaveTextContent('1');

        expect(withinSecondMatch.getByTestId('home-country')).toHaveTextContent('Canada');
        expect(withinSecondMatch.getByTestId('home-score')).toHaveTextContent('1');
        expect(withinSecondMatch.getByTestId('away-country')).toHaveTextContent('France');
        expect(withinSecondMatch.getByTestId('away-score')).toHaveTextContent('1');
    });
});
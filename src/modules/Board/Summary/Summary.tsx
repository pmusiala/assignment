import type { Match } from '../../../interfaces/Match';
import MatchScore from '../MatchScore/MatchScore';

type Props = {
    matchList: Array<Match>
}

const Summary: React.FC<Props> = ({ matchList }) => {
    return (<>
        {matchList.map((match, index) => (
            <div data-testid={`match-${index}`}
                className="grid grid-cols-7 gap-2 items-center p-2"
                key={match.id}>
                <MatchScore match={match} />
            </div>
        ))}
    </>);
};

export default Summary;
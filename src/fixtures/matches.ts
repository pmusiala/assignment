import { Match } from '../interfaces/Match';

export const firstMatch: Match = {
    id: '1',
    home: {
        countryCode: 'DE',
        score: 2
    },
    away: {
        countryCode: 'BR',
        score: 1
    }
};
export const secondMatch: Match = {
    id: '2',
    home: {
        countryCode: 'CA',
        score: 1
    },
    away: {
        countryCode: 'FR',
        score: 1
    },
};
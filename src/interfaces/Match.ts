import { CountryCode } from '../modules/Countries/countries';

export type Match = {
    id: string;
    home: {
        countryCode: CountryCode;
        score: number;
    },
    away: {
        countryCode: CountryCode;
        score: number;
    }
};

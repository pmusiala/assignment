import type { Meta, StoryObj } from '@storybook/react';
import Summary from '../modules/Board/Summary/Summary';
import { firstMatch, secondMatch } from '../fixtures/matches';

const meta = {
  title: 'Summary',
  component: Summary,
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
} satisfies Meta<typeof Summary>;

export default meta;

type Story = StoryObj<typeof meta>;

export const SummaryWithMultipleMatches: Story = {
  args: {
    matchList: [firstMatch, secondMatch]
  }
};


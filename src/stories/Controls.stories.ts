import type { Meta, StoryObj } from '@storybook/react';
import Controls from '../modules/Board/Controls/Controls';
import { firstMatch, secondMatch } from '../fixtures/matches';

const meta = {
  title: 'Controls',
  component: Controls,
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
} satisfies Meta<typeof Controls>;

export default meta;

type Story = StoryObj<typeof meta>;

export const ControlsWithMultipleMatches: Story = {
  args: {
    matchList: [firstMatch, secondMatch]
  }
};


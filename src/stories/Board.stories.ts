import type { Meta, StoryObj } from '@storybook/react';
import Board from '../modules/Board/Board';
import { firstMatch, secondMatch } from '../fixtures/matches';

const meta = {
  title: 'Board',
  component: Board,
  parameters: {
    // More on how to position stories at: https://storybook.js.org/docs/react/configure/story-layout
    layout: 'fullscreen',
  },
} satisfies Meta<typeof Board>;

export default meta;

type Story = StoryObj<typeof meta>;

export const BoardWithMockedData: Story = {
  args: {
    initialData: {
      [firstMatch.id]: { ...firstMatch, creationDate: Date.now() - 1 },
      [secondMatch.id]: { ...secondMatch, creationDate: Date.now() }
    }
  }
};


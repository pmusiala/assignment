import Board from './modules/Board/Board';
import ErrorBoundary from './modules/ErrorBoundary/ErrorBoundary';

function App() {
  return (
    <ErrorBoundary>
      <Board initialData={{}}></Board>
    </ErrorBoundary>
  );
}

export default App;

# assignment

## Assumptions

1. On the board I display two list of matches. Since summary changes order of matches I decided to create some kind of control panel. Reasoning behind it is better user experiance, essentially buttons won't change their position after clicks. 
1. This is world cup so in the domain I assume there is only country as a team. One team cannot play two matches at the same time. 
1. I didn't limit country number. You can use any country that was available in database I used.
1. There is some dynamic sorting and filtering that could be cached. Since right now it doesn't cause any trouble I decided to not preoptimise. 
1. No additional state management like recoil, jotai or even redux. I think simple `useReducer` is enough for such application. 
1. Trivial components are not tested. I could add snapshot test but I personally don't like them. VR testing would be really nice for this application. I didn't setup them due to limited time. 

## Getting started

Application is setup with vite. Reasoning behind using vite (instead webpack for instance) is faster development and better experiance. To run it you need to have node >= 18 and npm. 

`npm install`
`npm run dev` - App should open on port 5173


You can run components overview with storybook by using `npm run storybook`. To see storybook just visit `localhost:6006`

### Docker 

If you don't have node but have docker then use:
`docker-compose up install`
`docker-compose up dev` or `docker-compose up storybook`